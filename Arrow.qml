import QtQuick 2.10

Item     {
    id: mainItem
    width: 100
    height: width/2
//    color: "green"
    property color color: "#9E9E9E"
    property alias arrowWidth: canvas.width
    property alias arrowHeight: canvas.height
    
    Canvas {
        id: canvas
        anchors.centerIn: parent

        height: parent.height
        width: parent.width

        antialiasing: true

        onPaint: {
            var ctx = canvas.getContext('2d')

            ctx.strokeStyle = mainItem.color
            ctx.lineWidth = canvas.height * 0.05
            ctx.beginPath()
            ctx.moveTo(canvas.width * 0.05, canvas.height)
            ctx.lineTo(canvas.width / 2, 0)
            ctx.lineTo(canvas.width * 0.95, canvas.height)
            ctx.stroke()
        }
    }
}
