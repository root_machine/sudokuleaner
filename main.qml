import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.4

Window {
    visible: true
    width: 960
    height: 540
    title: qsTr("Sudoku")
    Item {
        id: name
        anchors.fill: parent
        MouseArea{
            anchors.fill: parent
            onClicked: {focus = true; console.log("should be defocused")}
        }

        Result{
            anchors.centerIn: parent
        }

//                Matrix{
//                    id: matrix
//                    anchors.centerIn: parent
//                }
        
        //        Row{
        //            anchors.horizontalCenter: parent.horizontalCenter
        //            anchors.top: matrix.bottom
        //            Button{
        //                text: "Set initial"
        //                onClicked: matrix.setState();
        //            }
        //            Button{
        //                text: "Make itteration"
        //                onClicked: matrix.makeItteration();
        //            }
        //            Button{
        //                text: "Clear"
        //                onClicked: matrix.clear();
        //            }
        //        }
    }
}
