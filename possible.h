#pragma once
#include <QVector>

class Possible
{
    QVector<QVector<bool>> vertical;
    QVector<QVector<bool>> horisontal;
    QVector<QVector<bool>> square;
    
    enum Form{ VERTICAL, HORISONTAL, SQUARE };
    
    QList<int> getPossibleForm(const int& x, const int& y, const Form& form);
    
public:
    Possible();
    void clear(); 
    void notify(int x, int y, int value);
    static int getSquareIndex(int x, int y); 
    QVector<int> getPossible(int x, int y) const; 
    void scanMatrix(QVector<QVector<int>> & matrix);
    QList<int> getPossibleInColumn(const int& x);
    QList<int> getPossibleInRow(const int& y);
    QList<int> getPossibleInRectangle(const int& x, const int& y);
    bool operator==(const Possible& cmp) const
    {
        return vertical == cmp.vertical && horisontal == cmp.horisontal && square == cmp.square;
    }
};
